
public class Zahlen {

	public static void main(String[] args) {
		// Schreiben Sie ein Programm �Zahlen�, in welchem ein ganzzahliges Array der L�nge 10
		//deklariert wird. Anschlie�end wird das Array mittels Schleife mit den Zahlen von 0 bis 9
		//gef�llt. Zum Schluss geben Sie die Elemente des Arrays wiederum mit einer Schleife auf der
		//Konsole aus.
		
		int[] zahlen = new int[10];
		
		for (int i = 0; i < zahlen.length; i++) {
			zahlen[i] = i;
		}
		
		for (int i = 0; i < zahlen.length; i++) {
			System.out.print(zahlen[i]);
		}
	}

}
