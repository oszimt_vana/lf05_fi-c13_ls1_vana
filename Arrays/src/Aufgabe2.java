
public class Aufgabe2 {

	public static void main(String[] args) {
		// Schreiben Sie eine Methode, die einen Parameter erh�lt: ein Feld mit ganzzahligen Werten.
		//Die Methode soll die Reihenfolge der Elemente in dem Feld umdrehen. Dabei darf kein
		//zus�tzliches Feld benutzt werden.
		//Hinweis: das erste Element muss mit dem letzten ausgetauscht werden, das zweite mit dem
		//vorletzten, ...
		int[] arr =  {1,2,3,4,5,6,7,8,9};
		tauschArray(arr);
		
	}
	public static void tauschArray(int[] zahlen) {
		int letzte = zahlen.length - 1;
		
		for (int erste = 0; erste < zahlen.length / 2; erste++) {
			int park = zahlen[erste];
			zahlen[erste] = zahlen[letzte];
			zahlen[letzte] = park;
			letzte --;
		}
		for (int index = 0; index < zahlen.length; index++) {
			System.out.print(zahlen[index]);
		}
	}
}
