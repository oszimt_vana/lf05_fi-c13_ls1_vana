
public class Aufgabe4 {

	public static void main(String[] args) {
		// Schreiben Sie eine Methode, die eine Tabelle mit Temperaturwerten in ein
		//zweidimensionales Feld ablegt. In einer Zeile der Tabelle sollen zwei Werte abgelegt
		//werden: ein Wert in Fahrenheit mit dem entsprechenden umgerechneten Wert in Celsius.
		//Dabei soll der erste Wert in Fahrenheit der Wert 0.0 sein, der zweite Wert in Fahrenheit
		//der Wert 10.0, ... Der Funktion soll die Anzahl der Zeilen, d.h. die Anzahl der zu
		//berechnenden Wertepaare als Parameter �bergeben werden. (Umrechnung von Fahrenheit
		//F nach Celsius C: C = (5/9) � (F � 32))
		
		tempTabelle(10);
	}
	public static void tempTabelle (int anzWerte) {
		double [] [] tab = new double [anzWerte] [2];
		double temp = 0.0;
		double teiler = 0.555555;
		
		for(int i = 0; i < anzWerte; i++) {
			tab[i] [0] = temp;
			tab[i] [1] = teiler * (tab[i] [0] - 32);
			temp += 10;
			System.out.printf("| %5.1f F |  | %5.1f C |\n", tab[i] [0], tab[i] [1]);
		}
	}
}
