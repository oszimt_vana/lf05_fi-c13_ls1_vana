import java.util.Arrays;

public class ArrayHelper {

	public static void main(String[] args) {
		// Schreiben Sie in der zu erstellenden Klasse ArrayHelper die Methode
		//public static String convertArrayToString(int[] zahlen). Die Methode soll zu
		//dem im Parameter �bergebenen Array eine mit Komma getrennte Auflistung der Werte als
		//String zur�ckgeben. Testen Sie Ihre Methode in der main-Methode
		int [] arr = new int[10];
		String str = convertArrayToString(arr);
		System.out.println(str);
		

	}
	public static String convertArrayToString(int[] zahlen) {
		String str = Arrays.toString(zahlen);
		return str;
	}
}
