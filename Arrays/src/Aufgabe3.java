
public class Aufgabe3 {

	public static void main(String[] args) {
		//Schreiben Sie wiederum eine Methode, die die Werte eines Feldes mit ganzzahligen Werten
		//umdreht (vgl. Aufgabe 2.1). Diesmal soll jedoch das Ergebnis in ein neues Feld abgelegt
		//werden. Dieses Feld soll R�ckgabewert der Methode sein
		int[] arr =  {1,2,3,4,5,6,7,8,9};
		tauschArray(arr);
	}
	public static int[] tauschArray(int[] zahlen) {
		int[] zahlen_neu = new int[zahlen.length];
		int index = 0;
		
		for (int i = zahlen.length -1; i >= 0; i --) {
			zahlen_neu[index] = zahlen[i];
			index ++;
		}
		for (int j = 0; j < zahlen_neu.length; j++) {
			System.out.print(zahlen_neu[j]);
		}
		return zahlen_neu;
	}
}
