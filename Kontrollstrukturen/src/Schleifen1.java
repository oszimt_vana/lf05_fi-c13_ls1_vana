import java.util.Scanner;

public class Schleifen1 {
	
	public static void aufgabe1() {

		Scanner myScanner = new Scanner(System.in);
		
		System.out.println("Geben sie eine Ganzzahl ein: ");
		int zahl = myScanner.nextInt();
		
		System.out.println("Möchten sie bis zu ihrer eingegebenen Zahl hochzählen, dann drücken sie '1'.");
		System.out.println("Möchten sie von ihrer eingegebenen Zahl runterzählen, dann drücken sie '0'.");
		int zählweise = myScanner.nextInt();
		
		if(zählweise == 1)
		{
			for(int n = 1; zahl >= n; n++) {
				System.out.print(" " + n);
			
			}
		}
		else if (zählweise == 0)
		{
			for(int n = zahl; n >= 1; n--) {
				System.out.print(" " + n);
			}
		}
		else {
			System.out.println("Die Eingabe ist ungültig!");
			System.out.println("Versuchen sie es erneut");
			aufgabe1();
		}
	}
	
	public static void aufgabe2a() {
		/*Geben Sie in der Konsole die Summe der Zahlenfolgen aus. Ermöglichen Sie es dem 
		Benutzer die Zahl n festzulegen, welche die Summierung begrenzt.  
		 
		 
		a)  1 + 2 + 3 + 4 +...+ n  for-Schleife / while-Schleife 
		b)  2 + 4 + 6 +...+ 2n  for-Schleife / while-Schleife 
		c) 1 + 3 + 5 +...+ (2n+1)  for-Schleife / while-Schleife */
		Scanner myScanner = new Scanner(System.in);
		
		System.out.println("Geben sie eine Ganzzahl ein: ");
		int zahl = myScanner.nextInt();
		int ergebnis = 0;
		
		for(int n = 1; n <= zahl; n++) {
			if(n < zahl)
			{
			System.out.print(n + " + ");
			ergebnis = ergebnis + n;
			}
			else if(n == zahl)
			{
				System.out.print(n);
			}
			
		}
		System.out.print(" = " + ergebnis);
		
	}
	
	public static void aufgabe2b() {
		/*b)  2 + 4 + 6 +...+ 2n  for-Schleife / while-Schleife */
		Scanner myScanner = new Scanner(System.in);
		System.out.println("Geben sie eine Granzzahl ein: ");
		int zahl = myScanner.nextInt();
		int ergebnis = 0;
		
		for(int n = 2; n <= zahl; n += 2) {
			if(n < zahl) 
			{
			System.out.print(n +  " + ");
			ergebnis = ergebnis + n;
			}
			else if(n == zahl)
			{
				System.out.print(n);
			}
		}
		System.out.print(" = " + ergebnis);
	}
	
	public static void aufgabe2c() {
		/*c) 1 + 3 + 5 +...+ (2n+1)  for-Schleife / while-Schleife */
		Scanner myScanner = new Scanner(System.in);
		
		System.out.println("Geben sie eine Ganzzahl ein: ");
		int zahl = myScanner.nextInt();
		int ergebnis = 0;
		
		for(int n = 1; n <= zahl; n = 2*n +1)
		{
			System.out.print(n + " + ");
			ergebnis = ergebnis + n;
		}
		System.out.print(" = " + ergebnis);
	}

	public static void aufgabe3() {
		/*Schreibe ein Programm, das für alle Zahlen zwischen 1 und 200 testet: 
			 
			▪ ob sie durch 7 teilbar sind. 
			▪ nicht durch 5 aber durch 4 teilbar sind. 
			 
			Lasse jeweils die Zahlen, auf die die Bedingungen zutreffen ausgeben*/
		for(int n = 1; n <= 200; n++) {
			if(n % 7 == 0) 
			{
				System.out.println("\nTeilbar durch 7: " + n);
			}
			else if(n % 5 != 0 && n % 4 == 0)
			{
				System.out.println("\nTeilbar durch 4 aber nicht durch 5: " + n);
			}
		}
	}
	
	public static void main(String[] args) {

		aufgabe3();
		
	}
}
