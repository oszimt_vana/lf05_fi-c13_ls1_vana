import java.util.Scanner;

public class Schleifen2 {

	public static void aufgabe3() {
		Scanner myScanner = new Scanner(System.in);
		
		System.out.println("Geben sie eine Ganzzahl kleinergleich 20 ein");
		int zahl = myScanner.nextInt();
		int ergebnis = 1;
		
		for(int n = 1; n <= zahl; n++) {
			ergebnis = ergebnis * n;
			System.out.print(" * " + n);
		}
		System.out.print(" = " + ergebnis);
	}
	public static void main(String[] args) {
		
		aufgabe3();
	}

}
