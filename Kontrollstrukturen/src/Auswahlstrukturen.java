import java.util.Scanner;

public class Auswahlstrukturen {

	public static void wennDann() {
		Scanner myScanner = new Scanner(System.in);
		System.out.println("Willkommen zur ersten �bung");
		System.out.println("Welche Aktivit�t m�chten sie zuerst tun?");
		System.out.println("Aufstehen oder losgehen");
		System.out.println("Geben sie 1 f�r Aufstehen und 0 f�r losgehen ein");
		int aktivit�t = myScanner.nextInt();
		
		if (aktivit�t == 1) 
		{
			System.out.println("Jetzt aber schnell!");
		}
		else if (aktivit�t == 0) 
		{
			System.out.println("Hast du dir schon Brotzeit gemacht?");
		}
		else 
		{
			System.out.println("Fehler!");
		}
				
	}

	public static void zahlenVergleich() {
		Scanner myScanner = new Scanner(System.in);
		
		System.out.println("Willkommen zur zweiten �bung");
		System.out.println("Geben sie eine Ganzzahl ein");
		int eingegebeneZahl1 = myScanner.nextInt();
		
		System.out.println("Geben sie eine zweite Ganzzahl ein");
		int eingegebeneZahl2 = myScanner.nextInt();
		
		if (eingegebeneZahl1 == eingegebeneZahl2)
		{
			System.out.println("Herzlichen Gl�ckwunsch die Zahlen sind gleich!");
		}
		if (eingegebeneZahl2 > eingegebeneZahl1)
		{
			System.out.println("Zahl 2 ist gr��er als Zahl1");
		}
		if (eingegebeneZahl1 > eingegebeneZahl2)
		{
			System.out.println("Zahl 1 ist gr��er als Zahl2");
		}
		else 
		{
			System.out.println("Die Zahlen sind unterschiedlich!");
		}
	}
	
	public static void dreiZahlen() {
		Scanner myScanner = new Scanner(System.in);
		
		System.out.println("\nWillkommen zur dritten �bung");
		System.out.println("Geben sie eine erste Ganzzahl ein");
		int zahl1 = myScanner.nextInt();
		
		System.out.println("Geben sie eine zweite Ganzzahl ein");
		int zahl2 = myScanner.nextInt();
		
		System.out.println("Geben sie eine dritte Ganzzahl ein");
		int zahl3 = myScanner.nextInt();
		
		if (zahl1 > zahl2 && zahl1 > zahl3)
		{
			System.out.println("Zahl 1 ist die gr��te Zahl");
		}
		if (zahl3 > zahl2 || zahl3 > zahl1)
		{
			System.out.println("Zahl 3 ist entweder gr��er als Zahl 1 oder als Zahl 2");
		}
		if (zahl2 > zahl1 && zahl2 > zahl3)
		{
			System.out.println("Zahl 2 ist die gr��te Zahl");
		}
		if (zahl3 > zahl1 && zahl3 > zahl2)
		{
			System.out.println("Zahl 3 ist die gr��te Zahl");
		}
		
		/*Wenn die 1.Zahl gr��er als die 2.Zahl und die 3. Zahl ist, soll eine Meldung ausgegeben werden (If mit && / Und)
		2. Wenn die 3.Zahl gr��er als die 2.Zahl oder die 1. Zahl ist, soll eine Meldung ausgegeben werden (If mit || / Oder)
		3. Geben Sie die gr��te der 3 Zahlen aus. (If-Else mit && / Und)*/
	}
	
	public static void main(String[] args) {
		
		wennDann();
		
		zahlenVergleich();
		
		dreiZahlen();
		
	}
}
