
public class Aufgabe1 {

	public static void main(String[] args) {
		int alter = 26;
		String name = "Philip";
		System.out.println("Mein Name ist " + name +" und ich bin " + alter + " Jahre alt.");
		System.out.print("Das hier sollte auf einer neuen \"Zeile\" stehen.");
		//Das ist ein Kommentar, bei "println" wird der String in einer neuen Zeile ausgegeben, bei "print" nicht;

	}

}
