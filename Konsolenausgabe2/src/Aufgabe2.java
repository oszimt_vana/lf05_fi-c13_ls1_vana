
public class Aufgabe2 {

	public static void main(String[] args) {
		
		System.out.printf("%-5s=%-19s=%5s", "0!", "", "1\n");
		System.out.printf("%-5s=%-19s=%5s", "1!", " 1", "1\n");
		System.out.printf("%-5s=%-19s=%5s", "2!", " 1 * 2", "2\n");
		System.out.printf("%-5s=%-19s=%5s", "3!", " 1 * 2 * 3", "6\n");
		System.out.printf("%-5s=%-19s=%5s", "4!", " 1 * 2 * 3 * 4", "24\n");
		System.out.printf("%-5s=%-19s=%5s", "5!", " 1 * 2 * 3 * 4 * 5", "120\n");
	}

}
