
public class Aufgabe3 {

	public static void main(String[] args) {
		
		//	Temperatur mit Nachkommastellen als Variabeln speichern
		double C1 = -28.8889;
		double C2 = -23.3333;
		double C3 = -17.7778;
		double C4 = -6.6667;
		double C5 = -1.1111;
		
		//	Erstelle String Variabeln
		String F = "Fahrenheit";
		String C = "Celsius";
		
		
		//	Gebe "Fahrenheit" linksbŁndig mit 10 Stellen bis "|" aus.
		//	Gebe "Celsius" rechtsbŁndig mit 9 Stellen aus.
		System.out.printf( "%-10s | %9s  \n" , F , C );
		System.out.print ("-----------------------\n");
		
		/*	Gebe "-20" linksbŁndig mit 10 Stellen bis "|" aus und
			gebe "C1" mit 2 Nachkommastellen rechtsbŁndig mit 9 Stellen aus.*/
		System.out.printf( "%-10s | %9.2f\n" ,   "-20" , C1);
		
		//	etc.
		System.out.printf( "%-10s | %9.2f\n" ,   "-10" , C2);
		System.out.printf( "%-10s | %9.2f\n" ,   "+0"  , C3);
		System.out.printf( "%-10s | %9.2f\n" ,   "+20" , C4);
		System.out.printf( "%-10s | %9.2f\n" ,   "+30" , C5);
		
	}

}
