import java.util.Scanner;

class Fahrkartenautomat_Endlos_testkopie{
	
    public static void main(String[] args){ 
    	
    	Scanner tastatur = new Scanner(System.in);
    	
    	double zuZahlenderBetrag; 
    	double r�ckgabebetrag;

   		int starten;
   		
		System.out.println("Ber�hren sie den Bildschrim, um fortzufahren! (1)");
   	   	starten = tastatur.nextInt();
   	   	
   	   	
   	   	
	   	while(starten == 1) {
   	   		zuZahlenderBetrag = 0;
   	   		  	   	
   	   		
   	   		zuZahlenderBetrag = fahkartenbestellungErfassen();
       
   	   		// Geldeinwurf
   	   		r�ckgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag);

   	   		// Fahrscheinausgabe
   	   		fahrkartenAusgeben();

   	   		// R�ckgeldberechnung und -Ausgabe
   	   		rueckgeldAusgeben(r�ckgabebetrag);

   	   		System.out.println(	"\nVergessen Sie nicht, den Fahrschein\n"+
                          		"vor Fahrtantritt entwerten zu lassen!\n"+
                          		"Wir w�nschen Ihnen eine gute Fahrt.\n");
   	   		
   	   		// Wahlweise Wiederholung des Kaufvorgangs
   	   		System.out.println("\nM�chten sie weitere Fahrscheine kaufen? (1)/(0)");
   	   		starten = tastatur.nextInt();
   	   		
   	   		if (starten == 0) {
   	   			System.out.println("\n-----------------Auf Wiedersehen!-----------------");
   	   		}
   	   		else 
   	   			starten = 1;
   	   		}
    }
		    
    public static double fahkartenbestellungErfassen() {
    	Scanner tastatur = new Scanner(System.in);
    	
    	double zuZahlenderBetrag = 0;
    	int fahrscheinwahl;
    	int anzahlTickets;
    	String[] bezeichnung = {
    							"Einzelfahrschein Berlin AB  (1)",
    							"Einzelfahrschein Berlin BC  (2)",
    							"Einzelfahrschein Berlin ABC (3)",
    							"Kurzstrecke	      (4)",
    							"Tageskarte Berlin AB  (5)",
    							"Tageskarte Berlin BC  (6)",
    							"Tageskarte Berlin ABC (7)",
    							"Kleingruppen-Tageskarte Berlin AB  (8) ",
    							"Kleingruppen-Tageskarte Berlin BC  (9) ",
    							"Kleingruppen-Tageskarte Berlin ABC (10)"
    							};
    	
    	Double[] preis = {2.90, 3.30, 3.60, 1.90, 8.60, 9.00, 9.60, 23.50, 24.30, 24.90};
    	
    	System.out.println("W�hlen sie eine Fahrkarte: \n" +
    					   "---------------------------------------------");
    	
    	for (int i = 0; i < bezeichnung.length; i++) {
    		System.out.println(bezeichnung [i] + " " +  "[" + preis[i] + " EUR]");
    	}
    	
    	fahrscheinwahl = tastatur.nextInt();
    	while(fahrscheinwahl < 1 || fahrscheinwahl > 10) {
    		System.out.println("falsche Eingabe");
    		System.out.print("Ihre Wahl: ");
    		fahrscheinwahl = tastatur.nextInt();
    	}
    	
    	switch(fahrscheinwahl) {
    	case 1:
    		zuZahlenderBetrag = preis[0];
    		break;
    	case 2:
    		zuZahlenderBetrag = preis[1];
    		break;
    	case 3:
    		zuZahlenderBetrag = preis[2];
    		break;
    	case 4:
    		zuZahlenderBetrag = preis[3];
    		break;
    	case 5:
    		zuZahlenderBetrag = preis[4];
    		break;
    	case 6:
    		zuZahlenderBetrag = preis[5];
    		break;
    	case 7:
    		zuZahlenderBetrag = preis[6];
    		break;
    	case 8:
    		zuZahlenderBetrag = preis[7];
    		break;
    	case 9:
    		zuZahlenderBetrag = preis[8];
    		break;
    	case 10:
    		zuZahlenderBetrag = preis[9];
    		break;
    	}
    	
    	System.out.print("Anzahl der Tickets: ");
    	anzahlTickets = tastatur.nextInt();
        if(anzahlTickets < 1 || anzahlTickets > 10) {
        	
        	anzahlTickets = 1;
        	System.out.println("Ung�ltige Ticketanzahl! Ticketanzahl wurde auf 1 gesetzt!");
        }
        
        return anzahlTickets * zuZahlenderBetrag;
    	
    }
    
    public static double fahrkartenBezahlen(double zuZahlen) {
    	Scanner tastatur = new Scanner(System.in);
    	
    	double eingezahlterGesamtbetrag = 0.0;
    	double eingeworfeneM�nze;
    	
    	while(eingezahlterGesamtbetrag < zuZahlen)
        {
     	   System.out.printf("\nNoch zu zahlen: %.2f Euro\n", (zuZahlen - eingezahlterGesamtbetrag));
     	   System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
     	   eingeworfeneM�nze = tastatur.nextDouble();
           eingezahlterGesamtbetrag += eingeworfeneM�nze;
        }
    	
    	return eingezahlterGesamtbetrag - zuZahlen;

    }
    
    public static void fahrkartenAusgeben() {
    	
    	System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++)
        {
        	System.out.print("=");
        	timer(250);
        }
        System.out.println("\n\n");
    }
    
    public static void rueckgeldAusgeben(double rueckgeld) {
    	
    	if(rueckgeld > 0.0)
        {
     	   System.out.printf("Der R�ckgabebetrag in H�he von %.2f EURO ", rueckgeld);
     	   System.out.println("wird in folgenden M�nzen ausgezahlt:");

            while(rueckgeld >= 2.0) // 2 EURO-M�nzen
            {
         	 muenzeAusgeben(2, " EURO");
         	 rueckgeld -= 2.0;
            }
            while(rueckgeld >= 1.0) // 1 EURO-M�nzen
            {
            	muenzeAusgeben(1, " EURO");
         	 rueckgeld -= 1.0;
            }
            while(rueckgeld >= 0.5) // 50 CENT-M�nzen
            {
            	muenzeAusgeben(50, " CENT");
         	 rueckgeld -= 0.5;
            }
            while(rueckgeld >= 0.2) // 20 CENT-M�nzen
            {
            	muenzeAusgeben(20, " CENT");
         	 rueckgeld -= 0.2;
            }
            while(rueckgeld >= 0.1) // 10 CENT-M�nzen
            {
            	muenzeAusgeben(10, " CENT");
         	 rueckgeld -= 0.1;
            }
            while(rueckgeld >= 0.05)// 5 CENT-M�nzen
            {
            	muenzeAusgeben(5, " CENT");
         	 rueckgeld -= 0.05;
            }
        }
    }
    
    public static void timer(int millisekunde) {
    	
    	try {
        	Thread.sleep(millisekunde);
           	} catch (InterruptedException e) {
 			e.printStackTrace();
           	}
    }
    
    public static void muenzeAusgeben(int betrag, String einheit) {
	   
	   System.out.println(betrag + einheit);
	   
   }
}
//Aufgabe 1: Es ist wegen der unterschiedlichen Datentypen nicht m�glich Preise und Bezeichnungen im selben Array zu speichern. Sollten sich die 
// 			 Preie �ndern, muss nur der "preise" Array angepasst werden, da die Bezeichnungen der Fahrkarten gleich bleiben. Das Programm ist 
//			 flexibler und l�sst sich ohne gro�en Aufwand anpassen.
//
//Aufgabe 3: Ein Vorteil der neuen Implementierung ist die einfache Erg�nzung der Arrays um neue Fahrkarten oder Preisanpassungen. Au�erdem wird die
// 			 Code-Struktur �bersichtlicher, da alle Bezeichnungen und Preise in den Arrays definiert sind und nicht dort, wo auf sie zugegriffen 
//			 werden soll, also an willk�rlichen Stellen im Code. Umso �fter ich auf die Arrays zugreifen muss, desto mehr schreibarbeit spare ich mir 
//			 mit der neuen Implementierung, da ich den Inhalt der Arrays nur einmal definieren muss.
//			 
