import java.util.Scanner;

class Fahrkartenautomat_Endlos{
	
    public static void main(String[] args){ 
    	
    	Scanner tastatur = new Scanner(System.in);
    	
    	double zuZahlenderBetrag; 
    	double r�ckgabebetrag;

   		int starten;
   		
		System.out.println("Ber�hren sie den Bildschrim, um fortzufahren! (1)");
   	   	starten = tastatur.nextInt();
   	   	
   	   	
   	   	
	   	while(starten == 1) {
   	   		zuZahlenderBetrag = 0;
   	   		  	   	
   	   		
   	   		zuZahlenderBetrag = fahkartenbestellungErfassen();
       
   	   		// Geldeinwurf
   	   		r�ckgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag);

   	   		// Fahrscheinausgabe
   	   		fahrkartenAusgeben();

   	   		// R�ckgeldberechnung und -Ausgabe
   	   		rueckgeldAusgeben(r�ckgabebetrag);

   	   		System.out.println(	"\nVergessen Sie nicht, den Fahrschein\n"+
                          		"vor Fahrtantritt entwerten zu lassen!\n"+
                          		"Wir w�nschen Ihnen eine gute Fahrt.\n");
   	   		
   	   		// Wahlweise Wiederholung des Kaufvorgangs
   	   		System.out.println("\nM�chten sie weitere Fahrscheine kaufen? (1)/(0)");
   	   		starten = tastatur.nextInt();
   	   		
   	   		if (starten == 0) {
   	   			System.out.println("\n-----------------Auf Wiedersehen!-----------------");
   	   		}
   	   		else 
   	   			starten = 1;
   	   		}
    }
		    
    public static double fahkartenbestellungErfassen() {
    	Scanner tastatur = new Scanner(System.in);
    	
    	double zuZahlenderBetrag = 0;
    	int fahrscheinwahl;
    	int anzahlTickets;

    	System.out.println("\nW�hlen Sie ihre Wunschfahrkarte f�r Berlin AB aus:\n" + 
    						"  Einzelfahrschein Regeltarif AB [2,90 EUR] (1)\n" + 
    						"  Tageskarte Regeltarif AB [8,60 EUR] (2)\n" + 
    						"  Kleingruppen-Tageskarte Regeltarif AB [23,50 EUR] (3)");
    	
    	System.out.print("\nIhre Wahl: ");
    	
    	fahrscheinwahl = tastatur.nextInt();
    	while(fahrscheinwahl < 1 || fahrscheinwahl > 3) {
    		System.out.println("falsche Eingabe");
    		System.out.print("Ihre Wahl: ");
    		fahrscheinwahl = tastatur.nextInt();
    	}
    	
    	switch(fahrscheinwahl) {
    	case 1:
    		zuZahlenderBetrag = 2.9;
    		break;
    	case 2:
    		zuZahlenderBetrag = 8.6;
    		break;
    	case 3:
    		zuZahlenderBetrag = 23.5;
    		break;
    	}
    	
    	System.out.print("Anzahl der Tickets: ");
    	anzahlTickets = tastatur.nextInt();
        if(anzahlTickets < 1 || anzahlTickets > 10) {
        	
        	anzahlTickets = 1;
        	System.out.println("Ung�ltige Ticketanzahl! Ticketanzahl wurde auf 1 gesetzt!");
        }
        
        return anzahlTickets * zuZahlenderBetrag;
    	
    }
    
    public static double fahrkartenBezahlen(double zuZahlen) {
    	Scanner tastatur = new Scanner(System.in);
    	
    	double eingezahlterGesamtbetrag = 0.0;
    	double eingeworfeneM�nze;
    	
    	while(eingezahlterGesamtbetrag < zuZahlen)
        {
     	   System.out.printf("\nNoch zu zahlen: %.2f Euro\n", (zuZahlen - eingezahlterGesamtbetrag));
     	   System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
     	   eingeworfeneM�nze = tastatur.nextDouble();
           eingezahlterGesamtbetrag += eingeworfeneM�nze;
        }
    	
    	return eingezahlterGesamtbetrag - zuZahlen;

    }
    
    public static void fahrkartenAusgeben() {
    	
    	System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++)
        {
        	System.out.print("=");
        	timer(250);
        }
        System.out.println("\n\n");
    }
    
    public static void rueckgeldAusgeben(double rueckgeld) {
    	
    	if(rueckgeld > 0.0)
        {
     	   System.out.printf("Der R�ckgabebetrag in H�he von %.2f EURO ", rueckgeld);
     	   System.out.println("wird in folgenden M�nzen ausgezahlt:");

            while(rueckgeld >= 2.0) // 2 EURO-M�nzen
            {
         	 muenzeAusgeben(2, " EURO");
         	 rueckgeld -= 2.0;
            }
            while(rueckgeld >= 1.0) // 1 EURO-M�nzen
            {
            	muenzeAusgeben(1, " EURO");
         	 rueckgeld -= 1.0;
            }
            while(rueckgeld >= 0.5) // 50 CENT-M�nzen
            {
            	muenzeAusgeben(50, " CENT");
         	 rueckgeld -= 0.5;
            }
            while(rueckgeld >= 0.2) // 20 CENT-M�nzen
            {
            	muenzeAusgeben(20, " CENT");
         	 rueckgeld -= 0.2;
            }
            while(rueckgeld >= 0.1) // 10 CENT-M�nzen
            {
            	muenzeAusgeben(10, " CENT");
         	 rueckgeld -= 0.1;
            }
            while(rueckgeld >= 0.05)// 5 CENT-M�nzen
            {
            	muenzeAusgeben(5, " CENT");
         	 rueckgeld -= 0.05;
            }
        }
    }
    
    public static void timer(int millisekunde) {
    	
    	try {
        	Thread.sleep(millisekunde);
           	} catch (InterruptedException e) {
 			e.printStackTrace();
           	}
    }
    
    public static void muenzeAusgeben(int betrag, String einheit) {
	   
	   System.out.println(betrag + einheit);
	   
   }
}

