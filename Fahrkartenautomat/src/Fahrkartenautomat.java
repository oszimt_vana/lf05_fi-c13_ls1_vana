import java.util.Scanner;

public class Fahrkartenautomat {
	
	    public static void main(String[] args)
	    {	    	
	       Scanner tastatur = new Scanner(System.in);
	     
	      
	       int anzahlFahrscheine; 					//Die Variable anzahlFarscheine wird als integer Datentyp deklariert, da hier nur Ganzzahlen eingegeben werden sollen.
	       float zuZahlenderBetrag; 				//Die Variable zuZahlenderBetrag wird als double Datentyp deklariert, da mit dieser Variable Berechnungen mit Gleitkommazahlen durchgef�hrt werden.
	       float eingezahlterGesamtbetrag;			//Die Variable eingezahlterGesamtbetrag wird als double Datentyp deklariert, da mit dieser Variable Berechnungen mit Gleitkommazahlen durchgef�hrt werden.
	       float eingeworfeneM�nze;					//Die Variable eingeworfeneM�nze wird als double Datentyp deklariert, da mit dieser Variable Berechnungen mit Gleitkommazahlen durchgef�hrt werden.
	       float r�ckgabebetrag;					//Die Variable r�ckgabebetrag wird als double Datentyp deklariert, da mit dieser Variable Berechnungen mit Gleitkommazahlen durchgef�hrt werden.
	       
	       
	       System.out.println("Zu zahlender Betrag (EURO): ");
	       zuZahlenderBetrag = tastatur.nextFloat();
	       
	       System.out.println("Wie viele Fahrscheine m�chten sie kaufen?");
	       anzahlFahrscheine = tastatur.nextInt();
	       
	       zuZahlenderBetrag = anzahlFahrscheine * zuZahlenderBetrag;			// Hier wird das Ergebnis aus der Multiplikation der Eingabewerte von anzahlFahrscheine und zuZahlenderBetrag der Variable zuZahlenderBetrag zugewiesen.
	       
	       // Geldeinwurf
	       // -----------
	       eingezahlterGesamtbetrag = (float) 0.0;
	       while(eingezahlterGesamtbetrag < zuZahlenderBetrag)					// Die Befehlsfolge zwischen den {} soll so lange wiederholt werden, bis die Bedingung, eingezahlterGesamtbetrag kleiner zuZahlenderBetrag, nicht mehr erf�llt ist. (Die Variablen werden verglichen)
	       {
	    	   System.out.printf("\nNoch zu zahlen: %.2f EURO",  (zuZahlenderBetrag - eingezahlterGesamtbetrag));
	    	   System.out.print("\nEingabe (mind. 5Ct, h�chstens 2 Euro): ");
	    	   eingeworfeneM�nze = tastatur.nextFloat();
	           eingezahlterGesamtbetrag += eingeworfeneM�nze;					// Der Wert von eingezahlterGesamtbetrag soll mit jedem Schleifendurchlauf um den Wert von eingeworfeneM�nze erh�ht werden, anschlie�end wird das Ergebnis eingezahlterGesamtbetrag zugewiesen
	       }

	       // Fahrscheinausgabe
	       // -----------------
	       System.out.println("\nFahrschein wird ausgegeben");
	       for (int i = 0; i < 8; i++)
	       {
	          System.out.print("=");
	          try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	       }
	       System.out.println("\n\n");

	       // R�ckgeldberechnung und -Ausgabe
	       // -------------------------------
	       r�ckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
	       if(r�ckgabebetrag > 0.0)
	       {
	    	   System.out.printf("Der R�ckgabebetrag in H�he von %.2f EURO " , r�ckgabebetrag);		//Hier wird die Ausgabe von r�ckgabebetrag auf zwei Nachkommastellen begrenzt
	    	   System.out.println("wird in folgenden M�nzen ausgezahlt:");

	           while(r�ckgabebetrag >= 2.0) // 2 EURO-M�nzen
	           {
	        	  System.out.println("2 EURO");
		          r�ckgabebetrag -= 2.0;
	           }
	           while(r�ckgabebetrag >= 1.0) // 1 EURO-M�nzen
	           {
	        	  System.out.println("1 EURO");
		          r�ckgabebetrag -= 1.0;
	           }
	           while(r�ckgabebetrag >= 0.5) // 50 CENT-M�nzen
	           {
	        	  System.out.println("50 CENT");
		          r�ckgabebetrag -= 0.5;
	           }
	           while(r�ckgabebetrag >= 0.2) // 20 CENT-M�nzen
	           {
	        	  System.out.println("20 CENT");
	 	          r�ckgabebetrag -= 0.2;
	           }
	           while(r�ckgabebetrag >= 0.1) // 10 CENT-M�nzen
	           {
	        	  System.out.println("10 CENT");
		          r�ckgabebetrag -= 0.1;
	           }
	           while(r�ckgabebetrag >= 0.0499)// 5 CENT-M�nzen
	           {
	        	  System.out.println("5 CENT");
	 	          r�ckgabebetrag -= 0.05;
	           }
	       }
	       
	       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
	                          "vor Fahrtantritt entwerten zu lassen!\n"+
	                          "Wir w�nschen Ihnen eine gute Fahrt.");
	       tastatur.close();
	    }
	    

}
// Aufgabe 5: Die Variable anzahlFarscheine wurde als integer Datentyp deklariert, da hier nur Ganzzahlen eingegeben werden sollen.
			//Die Variable zuZahlenderBetrag wurde als float Datentyp deklariert, da mit dieser Variable Berechnungen mit Gleitkommazahlen durchgef�hrt werden und 8 Dezimalastellen ausreichend sind.
			//Die Variable eingezahlterGesamtbetrag wurde als float Datentyp deklariert, da mit dieser Variable Berechnungen mit Gleitkommazahlen durchgef�hrt werden und 8 Dezimalastellen ausreichend sind.
			//Die Variable eingeworfeneM�nze wurde als float Datentyp deklariert, da mit dieser Variable Berechnungen mit Gleitkommazahlen durchgef�hrt werden und 8 Dezimalastellen ausreichend sind.
	    	//Die Variable r�ckgabebetrag wurde als float Datentyp deklariert, da mit dieser Variable Berechnungen mit Gleitkommazahlen durchgef�hrt werden und 8 Dezimalastellen ausreichend sind.

// Aufgabe 6: Hier wird Variable zuZahlenderBetrag das Ergebnis aus der Multiplikation der Eingabewerte von anzahlFahrscheine und zuZahlenderBetrag zugewiesen.

