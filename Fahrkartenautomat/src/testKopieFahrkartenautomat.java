import java.util.Scanner;

public class testKopieFahrkartenautomat {


   public static Double fahrkartenbestellungErfassen() {
	   
	   //Wieviel sollen die Fahrscheine kosten?
	   Scanner tastatur = new Scanner(System.in);
	   double zuZahlenderBetrag;
	   System.out.println("Zu zahlender Betrag (EURO): ");
       zuZahlenderBetrag = tastatur.nextFloat();
       
       //Anzahl der Fahrscheine
	   int anzahlFahrscheine;
       System.out.println("Wie viele Fahrscheine m�chten sie kaufen?");
       anzahlFahrscheine = tastatur.nextInt();
       
       //Ermittlung des zu zahlenden Betrags
       zuZahlenderBetrag = anzahlFahrscheine * zuZahlenderBetrag;
       
       return zuZahlenderBetrag;
       
   }
   
    public static Double fahrkartenBezahlen(double zuZahlen) {
    	// Geldeinwurf
	    // -----------
    	Scanner tastatur = new Scanner(System.in);
    	
    	double eingezahlterGesamtbetrag;
    	double zuZahlenderBetrag = zuZahlen;
    	double eingeworfeneM�nze;
    	
	    eingezahlterGesamtbetrag = (float) 0.0;
	    
	    while(eingezahlterGesamtbetrag < zuZahlenderBetrag)	{				
	    	   System.out.printf("\nNoch zu zahlen: %.2f EURO",  (zuZahlenderBetrag - eingezahlterGesamtbetrag));
	    	   System.out.print("\nEingabe (mind. 5Ct, h�chstens 2 Euro): ");
	    	   eingeworfeneM�nze = tastatur.nextFloat();
	           eingezahlterGesamtbetrag += eingeworfeneM�nze;
	    }
	    
	    double r�ckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
	    
	    tastatur.close();
	    
	    return r�ckgabebetrag;
	    
	       }

    
    public static void fahrkartenAusgeben() {
    	
    	// Fahrscheinausgabe
	       // -----------------
	       System.out.println("\nFahrschein wird ausgegeben");
	       for (int i = 0; i < 8; i++)
	       {
	          System.out.print("=");
	          try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
	       }
	       System.out.println("\n\n");
    }
    
    public static double rueckgeldAusgeben(double zuZahlen) {
    	
    		// R�ckgeldberechnung und -Ausgabe
	       // -------------------------------
	       double r�ckgabebetrag = zuZahlen;
	       
	       if(r�ckgabebetrag > 0.0) {
	    	   System.out.printf("Der R�ckgabebetrag in H�he von %.2f EURO " , r�ckgabebetrag);		//Hier wird die Ausgabe von r�ckgabebetrag auf zwei Nachkommastellen begrenzt
	    	   System.out.println("wird in folgenden M�nzen ausgezahlt:");

	           while(r�ckgabebetrag >= 2.0) // 2 EURO-M�nzen
	           {
	        	  System.out.println("2 EURO");
		          r�ckgabebetrag -= 2.0;
	           }
	           while(r�ckgabebetrag >= 1.0) // 1 EURO-M�nzen
	           {
	        	  System.out.println("1 EURO");
		          r�ckgabebetrag -= 1.0;
	           }
	           while(r�ckgabebetrag >= 0.5) // 50 CENT-M�nzen
	           {
	        	  System.out.println("50 CENT");
		          r�ckgabebetrag -= 0.5;
	           }
	           while(r�ckgabebetrag >= 0.2) // 20 CENT-M�nzen
	           {
	        	  System.out.println("20 CENT");
	 	          r�ckgabebetrag -= 0.2;
	           }
	           while(r�ckgabebetrag >= 0.1) // 10 CENT-M�nzen
	           {
	        	  System.out.println("10 CENT");
		          r�ckgabebetrag -= 0.1;
	           }
	           while(r�ckgabebetrag >= 0.0499)// 5 CENT-M�nzen
	           {
	        	  System.out.println("5 CENT");
	 	          r�ckgabebetrag -= 0.05;
	           }
    		}
	        return zuZahlen;  
    }
    

		
    public static void main(String[] args) {	    	
    	
    	double zuZahlenderBetrag;
    	double rueckgabeBetrag;
    	
	    zuZahlenderBetrag = fahrkartenbestellungErfassen();
	    rueckgabeBetrag = fahrkartenBezahlen(zuZahlenderBetrag);
	    fahrkartenAusgeben();
	    rueckgeldAusgeben(rueckgabeBetrag);
	    
	    System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                "vor Fahrtantritt entwerten zu lassen!\n"+
                "Wir w�nschen Ihnen eine gute Fahrt.");
	    
	       
	    }
	    

}
