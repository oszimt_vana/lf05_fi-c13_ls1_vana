import java.util.Scanner;
public class Ab_Schleifen1_Aufgabe8 {

	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);
		
		int seitenL;
		
		System.out.println("Welche Seitenlšnge soll das Quadrat haben?");
		seitenL = myScanner.nextInt();
		
		for (int i = 1; i <= seitenL; i++)
		{
			for (int j = 1; j <= seitenL; j++)
			{
				if (i == 1 || i == seitenL || j == 1 || j == seitenL ){
					System.out.print("* ");
				}
				else
				{
					System.out.print("  ");
				}
			}
			System.out.println();
		}
	}

}
