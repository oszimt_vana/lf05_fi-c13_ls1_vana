import java.util.Scanner;
public class Ab_Auswahlstrukturen_Aufgabe8 {

	public static void main(String[] args) {
		
		//Aufgabenstellung
		//Entwickeln Sie ein Programm, welches ausgibt,
		//ob das eingegebene Jahr ein Schaltjahr ist

		//Regel1: Wenn Jahreszahl teilbar durch 4, dann ist es ein Schaltjahr mit 366 Tagen
		
		//Regel2: Ausgenommen alle Jahreszahlen die auch teilbar durch 100 sind
		
		//Regel3: wenn nach Regel2 kein Schaltjahr, aber teilbar durch 400 dann Schaltjahr
		//-----------------------------------------------------------------------------------------------
		
		//Erfrage eine Jahreszahl
		Scanner myScanner = new Scanner(System.in);
		
		int jahreszahl;
		int schaltjahr;
		
		System.out.println("Geben sie eine Jahreszahl ein, um zu pr�fen, ob es ein Schaltjahr ist");
		jahreszahl = myScanner.nextInt();
		
		//Dummerweise wurde diese Regelung erst 1582 eingef�hrt, d.h. davor galt nur Regel1
		//Pr�fe auf Schaltjahr (Wenn Regel 1 zutrifft und Regel 2 nicht)
		if(jahreszahl < 1582) {
			
			if(jahreszahl % 4 == 0) {
					System.out.println("Das Jahr ist ein Schaltjahr");
			}
			else {
				System.out.println("Das Jahr ist kein Schaltjahr");
			}
		}
		else {
			if((jahreszahl % 4 == 0) && (jahreszahl % 100 > 0)) {
				System.out.println("Das Jahr ist ein Schaltjahr");
			}
			else {
				if (jahreszahl % 400 == 0) {
					System.out.println("Das Jahr ist ein Schaltjahr");
				}
				else {
					System.out.println("Das Jahr ist kein Schaltjahr");
				}
			}
		}
	}
}


	