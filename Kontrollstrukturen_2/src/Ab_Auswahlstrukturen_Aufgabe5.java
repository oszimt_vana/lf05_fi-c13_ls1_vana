import java.util.Scanner;

public class Ab_Auswahlstrukturen_Aufgabe5 {

	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);
		
		//Formel:
		//BMI = K�rpergewicht / K�rpergr��e^2
		//-------------------------

		//Aufgabenstellung:
		//Das Programm soll vom Benutzer das Gewicht [in kg] die Gr��e [in cm] und das Geschlecht 
		//[m/w] abfragen. Am Ende des Programms soll die BMI-Klassifikation der Person ausgegeben 
		//werden.
		//-------------------------
		
		//Deklariere Varaiblen
		//---------------------------
		double gewicht;
		double gr��e;
		char geschlecht;
		double bmi;
		
		//Erfrage Eingabe
		//---------------------------
		System.out.println("Willkommen zum BMI-Rechner!");
		System.out.println("Sind sie eine Frau, oder ein Mann? [w/m]");
		geschlecht = myScanner.next().charAt(0);
		
		System.out.println("Geben sie ihr K�rpergewicht in kg ein: ");
		gewicht = myScanner.nextDouble();
		
		System.out.println("Geben sie ihre Gr��e in cm ein: ");
		gr��e = myScanner.nextDouble();
		gr��e = gr��e/100;
		
		//Errechne BMI
		//---------------------------
		//Tabelle:
		//Klassifikation m w 
		//Untergewicht <20 <19 
		//Normalgewicht 20-25 19-24 
		//�bergewicht >25 >24
		//---------------------------
		bmi = gewicht / (gr��e * gr��e);
		
		
		//Bestimmung Ausgabewerte weiblich
		//---------------------------
		if(geschlecht == 'w') {
			if(bmi < 19) {
				System.out.println("Sie haben Untergewicht und sollten mehr essen.");
			}
			if(bmi >= 19 && bmi <=24) {
				System.out.println("Sie haben Normalgewicht!");
			}
			if(bmi > 24) {
				System.out.println("Sie haben �bergewicht! Sie sollten sich mehr bewegen!");
			}
		}
		
		//Bestimmung Ausgabewerte weiblich
		//---------------------------
		if(geschlecht == 'm') {
			if(bmi < 20) {
				System.out.printf("Sie haben Untergewicht und sollten mehr essen.");
			}
			if(bmi >= 20 && bmi <=25) {
				System.out.println("Sie haben Normalgewicht!");
			}
			if(bmi > 25) {
				System.out.println("Sie haben �bergewicht! Sie sollten sich mehr bewegen!");
			}
		}
			System.out.println("\nIhr BMI ist: ");
			System.out.printf("%.2f", bmi);
	}

}
