import java.util.Scanner;
public class Ab_Schleifen2_Aufgabe6 {

	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);
		
		double einlage = 0;
		double zinssatz = 0;
		int jahre = 0;
		char antwort;
		
		System.out.println("Wie hoch soll ihre Einlage sein?");
		einlage = myScanner.nextDouble();
		
		System.out.println("Wie hoch ist der Zinssatz?");
		zinssatz = myScanner.nextDouble();
		
		while (einlage < 1000000) {
			einlage = einlage + (einlage * zinssatz);
			jahre += 1;
		}
		System.out.println("Es dauert " + jahre + " bis sie Million�r sind!");
		System.out.println("M�chten sie eine erneute Berechnung durchf�hren? [j]/[n]");
		antwort = myScanner.next().charAt(0);
		
		if(antwort == 'j') {
			main( args);
		}
		else {
			System.out.println("Auf Wiedersehen!");
		}
	}

}
