import java.util.Scanner;
public class Ab_Fallunterscheidung_Aufgabe4 {

	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);
		
		//Deklariere Eingabezahlen und Rechenoperator
		double zahl1;
		double zahl2;
		double ergebnis = 0;
		char operator;
		
		//Erfrage Eingabezahlen und Rechenoperator
		System.out.println("Geben sie die erste Zahl ein: ");
		zahl1 = myScanner.nextDouble();
		
		System.out.println("Geben sie die zweite Zahl ein: ");
		zahl2 = myScanner.nextDouble();
		
		System.out.println("Geben sie den Rechenoperator [+, -, *, /]  ein: ");
		operator = myScanner.next().charAt(0);
		
		switch(operator) 
		{
		case '+':
			ergebnis = zahl1 + zahl2;
			break;
		case '-':
			ergebnis = zahl1 - zahl2;
			break;
		case '*':
			ergebnis = zahl1 * zahl2;
			break;
		case '/':
			ergebnis = zahl1 / zahl2;
			break;
		default:
			System.out.println("Fehlerhafte eingabe!");
		}
		
		System.out.println(zahl1 + " " + operator + " " + zahl2 + " = " + ergebnis);
	}

}
	