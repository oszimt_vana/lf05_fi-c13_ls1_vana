import java.util.Scanner;
public class Ab_Fallunterscheidung_Aufgabe5 {

	public static void main(String[] args) {
		/*	Schreiben Sie ein Programm, in das der Benutzer zun�chst �ber die Eingabe der
		 	Buchstaben R, U oder I ausw�hlen kann, welche Gr��e berechnet werden soll. Gibt er einen 
			falschen Buchstaben ein, soll eine Meldung �ber die Fehleingabe erfolgen. 
			Anschlie�end soll er die Werte der fehlenden Gr��en eingeben. Am Ende gibt das 
			Programm den Wert der gesuchten Gr��e mit der richtigen Einheit aus.*/
		
		Scanner myScanner = new Scanner(System.in);
		
		
		char gesucht;
		double ergebnis = 0;
		double r;
		double u;
		double i;
		
		System.out.println("M�chten sie den Widerstant [R], die Spannung [U] oder die Stromst�rke [I] berechnen?");
		gesucht = myScanner.next().charAt(0);
		
		switch(gesucht) 
		{
		case 'R':
			System.out.println("Geben sie die Stromst�rke I an: ");
			i = myScanner.nextDouble();
			System.out.println("Geben sie die Spanning U an: ");
			u = myScanner.nextDouble();
			ergebnis = u / i;
			break;
		case 'U':
			System.out.println("Geben sie die Stromst�rke I an: ");
			i = myScanner.nextDouble();
			System.out.println("Geben sie den Widerstand R an: ");
			r = myScanner.nextDouble();
			ergebnis = r * i;
			break;
		case 'I':
			System.out.println("Geben sie den Widerstand R an: ");
			r = myScanner.nextDouble();
			System.out.println("Gebne sie die Spannung U an: ");
			u = myScanner.nextDouble();
			ergebnis = u / r;
			break;
		default:
			System.out.println("Fehlerhafte Eingabe ");
		}
		System.out.println("Die gesuchte Gr��e ist " + ergebnis);
	}

}
