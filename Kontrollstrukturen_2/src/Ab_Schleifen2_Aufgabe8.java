import java.util.Scanner;

public class Ab_Schleifen2_Aufgabe8 {

	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);
		
		//initialisiere Variablen
		int counter = 1;
		int eingabezahl;
			
		System.out.println("Geben sie eine Zahl zwischen 2 und 9 ein: ");
		eingabezahl = myScanner.nextInt();
		
		while (counter <= 100) {

		
			for (int i = 0; i < 10; i++) {
				int quersumme1 = counter % 10;
				int quersumme2= counter / 10;
				
				if ((counter % eingabezahl == 0) || (quersumme1 + quersumme2 == eingabezahl)){
					System.out.printf("%4s", "*");
				}
				else if ((quersumme1 == eingabezahl) || (quersumme2 == eingabezahl)) {
					System.out.printf("%4s", "*");
				}
				else {
					System.out.printf("%4s", counter);
				}
				
				counter ++;
			}
			System.out.println();
		}
	}
}